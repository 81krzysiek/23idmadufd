<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\CountryContract;
use App\Domain\Models\Country;

class CountryRepository implements CountryContract
{
    public function findNyCode(string $code): ?Country
    {
        return Country::where('code', $code)->first();
    }
}
