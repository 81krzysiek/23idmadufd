<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\EmployeeContract;
use App\Domain\Models\Employee;

class EmployeeRepository implements EmployeeContract
{
    public function create(array $data): Employee
    {
        return Employee::create($data);
    }

    public function getByIdentifier(string $identifier): ?Employee
    {
        return Employee::where('identifier', $identifier)->first();
    }

}
