<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\DelegationItemContract;
use App\Domain\Models\DelegationItem;

class DelegationItemRepository implements DelegationItemContract
{
    public function create(array $data): void
    {
        DelegationItem::insert($data);
    }

    public function findInterval(string $identifier, string $start, string $end): ?DelegationItem
    {
        return DelegationItem::join('delegations', 'delegations.id', '=', 'delegation_items.delegation_id')
            ->join('employees', 'delegations.employee_id', '=', 'employees.id')
            ->where('employees.identifier', $identifier)
            ->where(function ($query) use ($start, $end) {
                $query->whereBetween('delegation_items.start', [$start, $end])
                    ->orWhereBetween('delegation_items.end', [$start, $end]);
            })
            ->first();
    }

}
