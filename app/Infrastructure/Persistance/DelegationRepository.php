<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistance;

use App\Domain\Contracts\DelegationContract;
use App\Domain\Models\Delegation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DelegationRepository implements DelegationContract
{
    public function create(array $data): Delegation
    {
        return Delegation::create($data);
    }

    public function findByEmployeeIdentifier(string $identifier): Collection
    {
        return Delegation::select(['delegations.start', 'delegations.end', 'countries.code as country', DB::raw('(SELECT SUM(amount) FROM delegation_items WHERE delegations.id = delegation_items.delegation_id) as amount'), 'delegations.currency'])
            ->join('employees', 'delegations.employee_id', 'employees.id')
            ->join('countries', 'delegations.country_id', 'countries.id')
            ->where('employees.identifier', $identifier)
            ->get();
    }

}
