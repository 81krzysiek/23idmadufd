<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Country;

use App\Domain\Contracts\CountryContract;
use App\Domain\Models\Country;

class CountryFindByCodeCommand
{
    private CountryContract $contract;

    public function __construct(CountryContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $code
     * @return Country|null
     */
    public function execute(string $code): ?Country
    {
        return $this->contract->findNyCode($code);
    }
}
