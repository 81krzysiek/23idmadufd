<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\DelegationItem;

use App\Domain\Contracts\DelegationItemContract;
use App\Domain\Models\DelegationItem;

class DelegationItemFindIntervalCommand
{
    private DelegationItemContract $contract;

    public function __construct(DelegationItemContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $identifier
     * @param string $start
     * @param string $end
     * @return DelegationItem|null
     */
    public function execute(string $identifier, string $start, string $end): ?DelegationItem
    {
        return $this->contract->findInterval($identifier, $start, $end);
    }
}
