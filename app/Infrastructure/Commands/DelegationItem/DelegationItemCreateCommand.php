<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\DelegationItem;

use App\Domain\Contracts\DelegationItemContract;

class DelegationItemCreateCommand
{
    private DelegationItemContract $contract;

    public function __construct(DelegationItemContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     * @return void
     */
    public function execute(array $data): void
    {
        $this->contract->create($data);
    }
}
