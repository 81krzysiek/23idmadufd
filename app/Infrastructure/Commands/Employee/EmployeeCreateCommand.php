<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Employee;

use App\Domain\Contracts\EmployeeContract;
use App\Domain\Models\Employee;

class EmployeeCreateCommand
{
    private EmployeeContract $contract;

    public function __construct(EmployeeContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     * @return Employee|null
     */
    public function execute(array $data): ?Employee
    {
        return $this->contract->create($data);
    }
}
