<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Employee;

use App\Domain\Contracts\EmployeeContract;
use App\Domain\Models\Employee;

class EmployeeFindByIdentifierCommand
{
    private EmployeeContract $contract;

    public function __construct(EmployeeContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $identifier
     * @return Employee|null
     */
    public function execute(string $identifier): ?Employee
    {
        return $this->contract->getByIdentifier($identifier);
    }
}
