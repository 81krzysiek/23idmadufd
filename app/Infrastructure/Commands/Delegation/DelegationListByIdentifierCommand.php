<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Delegation;

use App\Domain\Contracts\DelegationContract;
use Illuminate\Database\Eloquent\Collection;

class DelegationListByIdentifierCommand
{
    private DelegationContract $contract;

    public function __construct(DelegationContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param string $identifier
     * @return Collection
     */
    public function execute(string $identifier): Collection
    {
        return $this->contract->findByEmployeeIdentifier($identifier);
    }
}
