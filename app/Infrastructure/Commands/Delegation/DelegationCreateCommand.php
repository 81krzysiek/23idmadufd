<?php
declare(strict_types=1);
namespace App\Infrastructure\Commands\Delegation;

use App\Domain\Contracts\DelegationContract;
use App\Domain\Models\Delegation;

class DelegationCreateCommand
{
    private DelegationContract $contract;

    public function __construct(DelegationContract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param array $data
     * @return Delegation
     */
    public function execute(array $data): Delegation
    {
        return $this->contract->create($data);
    }
}
