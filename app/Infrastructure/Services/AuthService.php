<?php
declare(strict_types=1);
namespace App\Infrastructure\Services;

use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;

class AuthService
{
    /**
     * @param string $key
     * @return string
     */
    public static function throttleKey(string $key): string
    {
        return Str::lower($key);
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function checkTooManyFailedAttempts(string $key): bool
    {
        if (! RateLimiter::tooManyAttempts(self::throttleKey($key), 5)) {
            return true;
        }

        return false;
    }
}
