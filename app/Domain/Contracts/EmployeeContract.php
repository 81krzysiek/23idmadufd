<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Employee;

interface EmployeeContract
{
    public function create(array $data): Employee;

    public function getByIdentifier(string $identifier): ?Employee;
}
