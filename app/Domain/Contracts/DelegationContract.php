<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Delegation;
use Illuminate\Database\Eloquent\Collection;

interface DelegationContract
{
    public function create(array $data): Delegation;

    public function findByEmployeeIdentifier(string $identifier): Collection;
}
