<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\DelegationItem;

interface DelegationItemContract
{
    public function create(array $data): void;

    public function findInterval(string $identifier, string $start, string $end): ?DelegationItem;
}
