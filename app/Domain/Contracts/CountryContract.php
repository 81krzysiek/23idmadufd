<?php
declare(strict_types=1);
namespace App\Domain\Contracts;

use App\Domain\Models\Country;

interface CountryContract
{
    public function findNyCode(string $code): ?Country;
}
