<?php
declare(strict_types=1);
namespace App\Domain\Traits;

use Illuminate\Support\Str;

trait UpperField
{
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->upperFields)) {
            $value = Str::upper($value);
        }

        parent::setAttribute($key, $value);
    }
}
