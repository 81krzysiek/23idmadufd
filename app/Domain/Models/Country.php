<?php
declare(strict_types=1);
namespace App\Domain\Models;

use App\Domain\Traits\UpperField;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use UpperField;
    protected $table = 'countries';

    protected $fillable = [
        'name',
        'code',
        'amount'
    ];

    protected array $upperFields = ['code'];
}
