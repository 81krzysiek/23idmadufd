<?php
declare(strict_types=1);
namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Delegation extends Model
{
    public static int $MIN_HOUR = 8;
    public static array $EXCEPTION_DAY = ['Sun', 'Sat'];
    public static int $MORE_THAN = 7;
    public static int $QUOTIENT = 2;
    protected $fillable = [
        'start',
        'end',
        'currency',
        'country_id',
        'employee_id'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function items()
    {
        return $this->hasMany(DelegationItem::class);
    }
}
