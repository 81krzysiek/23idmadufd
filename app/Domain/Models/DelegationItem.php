<?php
declare(strict_types=1);
namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class DelegationItem extends Model
{
    protected $fillable = [
        'start',
        'end',
        'amount',
        'delegation_id'
    ];

    public function delegation()
    {
        return $this->belongsTo(Delegation::class, 'delegation_id', 'id');
    }
}
