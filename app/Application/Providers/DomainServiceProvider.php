<?php
declare(strict_types=1);
namespace App\Application\Providers;

use App\Domain\Contracts\CountryContract;
use App\Domain\Contracts\DelegationContract;
use App\Domain\Contracts\DelegationItemContract;
use App\Domain\Contracts\EmployeeContract;
use App\Infrastructure\Persistance\CountryRepository;
use App\Infrastructure\Persistance\DelegationItemRepository;
use App\Infrastructure\Persistance\DelegationRepository;
use App\Infrastructure\Persistance\EmployeeRepository;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap domain application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register domain application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmployeeContract::class, EmployeeRepository::class);
        $this->app->bind(DelegationContract::class, DelegationRepository::class);
        $this->app->bind(DelegationItemContract::class, DelegationItemRepository::class);
        $this->app->bind(CountryContract::class, CountryRepository::class);
    }
}
