<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiExternal\Request;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeCreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        $identifierValid = ['required', 'string'];
        if($this->get('person')){
            $identifierValid = ['required', 'string', 'unique:employees,identifier'];
        }
        return [
            'identifier' => $identifierValid,
            'person' => 'nullable|string|max:255',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge(json_decode($this->getContent(), true) ?? []);
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ]));
    }
}
