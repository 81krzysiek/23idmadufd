<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiExternal\Action;

use App\Domain\Models\Country;
use App\Domain\Models\Delegation;
use App\Infrastructure\Commands\Country\CountryFindByCodeCommand;
use App\Infrastructure\Commands\Delegation\DelegationCreateCommand;
use App\Infrastructure\Commands\DelegationItem\DelegationItemCreateCommand;
use App\Infrastructure\Commands\DelegationItem\DelegationItemFindIntervalCommand;
use App\Infrastructure\Commands\Employee\EmployeeFindByIdentifierCommand;
use App\Interfaces\Http\ApiExternal\Request\DelegationCreateRequest;
use App\Interfaces\Http\BaseAction;
use Carbon\Carbon;

class DelegationCreateAction extends BaseAction
{
    private DelegationItemFindIntervalCommand $delegationItemFindIntervalCommand;
    private EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand;
    private DelegationCreateCommand $delegationCreateCommand;
    private DelegationItemCreateCommand $delegationItemCreateCommand;
    private CountryFindByCodeCommand $countryFindByCodeCommand;

    public function __construct(
        DelegationItemFindIntervalCommand $delegationItemFindIntervalCommand,
        EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand,
        DelegationCreateCommand $delegationCreateCommand,
        DelegationItemCreateCommand $delegationItemCreateCommand,
        CountryFindByCodeCommand $countryFindByCodeCommand
    )
    {
        $this->delegationItemFindIntervalCommand = $delegationItemFindIntervalCommand;
        $this->employeeFindByIdentifierCommand = $employeeFindByIdentifierCommand;
        $this->delegationCreateCommand = $delegationCreateCommand;
        $this->delegationItemCreateCommand = $delegationItemCreateCommand;
        $this->countryFindByCodeCommand = $countryFindByCodeCommand;
    }

    public function __invoke(DelegationCreateRequest $request)
    {
        $data = $request->validated();
        $prepareData = $this->prepareData($data);

        if(count($errors = $this->checkDays($prepareData, $data['identifier']))){

            return response()->json(['success' => false, 'message' => 'invalid_data', 'data' => $errors], 410);
        }
        $country = $this->countryFindByCodeCommand->execute($data['code']);
        $delegation = $this->delegationCreateCommand->execute(
            [
                'employee_id' => $this->employeeFindByIdentifierCommand->execute($data['identifier'])->id,
                'start' => $data['start'],
                'end' => $data['end'],
                'country_id' => $country->id,
            ]);

        $this->delegationItemCreateCommand->execute($this->prepareDelegationItems($prepareData, $country, $delegation));

        return response()->json(['success' => true, 'message' => ''], 201);

    }

    /**
     * @param array $data
     * @return array
     */
    public function prepareData(array $data): array
    {
        $days = [];

        $from = Carbon::parse($data['start']);
        $to = Carbon::parse($data['end']);
        $diffInDay = $to->diffInDays($from);

        if($diffInDay === 0){
            $days[] = ['start' => $data['start'], 'end' => $data['end'], 'isPay' => $this->isPay($data['start'], $data['end'])];

        }else{
            $days[] = ['start' => $data['start'], 'end' => $from->format("Y-m-d"). ' 23:59:59', 'isPay' => $this->isPay($data['start'], $from->format("Y-m-d"). ' 23:59:59')];
            $days[] = ['start' => $to->format("Y-m-d"). ' 00:00:00', 'end' => $data['end'], 'isPay' => $this->isPay($to->format("Y-m-d"). ' 00:00:00', $data['end'])];
        }

        for ($i=1; $i < $diffInDay; $i++){
            $day = $from->addDay();
            $days[] = ['start' => $day->format("Y-m-d").' 00:00:00', 'end' => $day->format("Y-m-d"). ' 23:59:59', 'isPay' => $this->isPay($day->format("Y-m-d").' 00:00:00', $day->format("Y-m-d"). ' 23:59:59')];
        }


        return $days;
    }

    /**
     * @param string $from
     * @param string $to
     * @return bool
     */
    private function isPay(string $from, string $to): bool
    {
        $from = Carbon::parse($from);
        $to = Carbon::parse($to);

        if(in_array($from->format('D'), Delegation::$EXCEPTION_DAY) || $to->diffInHours($from) < Delegation::$MIN_HOUR){

            return false;
        }

        return true;
    }

    /**
     * @param $prepareData
     * @return array
     */
    private function checkDays($prepareData, string $identifier): array
    {
        $errors = [];
        foreach ($prepareData as $row){
            $delegationDay = $this->delegationItemFindIntervalCommand->execute($identifier, $row['start'], $row['end']);

            if($delegationDay){
                $errors[] = $row;
            }
        }

        return $errors;
    }

    /**
     * @param array $prepareData
     * @param Country $country
     * @param Delegation $delegation
     * @return array
     */
    public function prepareDelegationItems(array $prepareData, Country $country, Delegation $delegation): array
    {
        $insertRows = [];
        foreach ($prepareData as $key => $row){
            $row['amount'] = $country->amount;
            $row['delegation_id'] = $delegation->id;
            if($key+1 > Delegation::$MORE_THAN){
                $row['amount'] = $country->amount * Delegation::$QUOTIENT;
            }
            $row['amount'] = match ($row['isPay']){
                false => 0,
                true => $row['amount']
            };
            unset($row['isPay']);
            $insertRows[] = $row;
        }

        return $insertRows;
    }
}
