<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiExternal\Action;

use App\Infrastructure\Commands\Employee\EmployeeCreateCommand;
use App\Infrastructure\Commands\Employee\EmployeeFindByIdentifierCommand;
use App\Interfaces\Http\ApiExternal\Request\EmployeeCreateRequest;
use App\Interfaces\Http\BaseAction;

class EmployeeCreateAction extends BaseAction
{
    private EmployeeCreateCommand $employeeCreateCommand;
    private EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand;

    public function __construct(EmployeeCreateCommand $employeeCreateCommand, EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand)
    {
        $this->employeeCreateCommand = $employeeCreateCommand;
        $this->employeeFindByIdentifierCommand = $employeeFindByIdentifierCommand;
    }

    public function __invoke(EmployeeCreateRequest $request)
    {
        $data = $request->validated();

        return match ($data['person'] ?? null){
            null => $this->getByIdentifier($data['identifier']),
            default =>  $this->createEmployee($data),
        };
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    private function createEmployee($data): \Illuminate\Http\JsonResponse
    {
        $this->employeeCreateCommand->execute($data);

        return response()->json(['success' => true, 'message' => 'create_user'], 201);
    }

    private function getByIdentifier(string $identifier)
    {
        if ($employee = $this->employeeFindByIdentifierCommand->execute($identifier)){

            return response()->json(['success' => true, 'identifier' => $employee->identifier], 200);
        }

        return response()->json(['success' => false, 'message' => 'not_find_identifier'], 410);
    }
}
