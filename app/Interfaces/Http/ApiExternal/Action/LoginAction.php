<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiExternal\Action;

use App\Infrastructure\Services\AuthService;
use App\Interfaces\Http\ApiExternal\Request\LoginRequest;
use App\Interfaces\Http\BaseAction;
use Illuminate\Support\Facades\RateLimiter;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginAction extends BaseAction
{
    public function __invoke(LoginRequest $request)
    {
        $credentials = $request->validated();

        if(false === AuthService::checkTooManyFailedAttempts($credentials['login'])){
            return response()->json(['success' => false, 'message' => 'could_not_create_token'], 500);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                RateLimiter::hit(AuthService::throttleKey($credentials['login']), $seconds = 300);
                return response()->json(['success' => false, 'message' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {

            RateLimiter::hit(AuthService::throttleKey($credentials['login']), $seconds = 300);
            return response()->json(['success' => false, 'message' => 'could_not_create_token'], 500);
        }
        RateLimiter::clear(AuthService::throttleKey($credentials['login']));

        return response()->json(['success' => true, 'token' => $token]);
    }
}
