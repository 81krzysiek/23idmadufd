<?php
declare(strict_types=1);
namespace App\Interfaces\Http\ApiExternal\Action;

use App\Infrastructure\Commands\Delegation\DelegationListByIdentifierCommand;
use App\Infrastructure\Commands\Employee\EmployeeFindByIdentifierCommand;
use App\Interfaces\Http\BaseAction;

class DelegationListByIdentifierAction extends BaseAction
{
    private DelegationListByIdentifierCommand $delegationListByIdentifierCommand;
    private EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand;

    public function __construct(DelegationListByIdentifierCommand $delegationListByIdentifierCommand, EmployeeFindByIdentifierCommand $employeeFindByIdentifierCommand)
    {
        $this->delegationListByIdentifierCommand = $delegationListByIdentifierCommand;
        $this->employeeFindByIdentifierCommand = $employeeFindByIdentifierCommand;
    }

    public function __invoke(string $identifier)
    {
        $employee = $this->employeeFindByIdentifierCommand->execute($identifier);

        if(!$employee){
            return response()->json(['success' => false, 'message' => 'invalid_data'], 410);
        }

        $delegations = $this->delegationListByIdentifierCommand->execute($identifier);

        return response()->json(['success' => true, 'message' => '', 'data' => $delegations->toArray()], 200);
    }
}
