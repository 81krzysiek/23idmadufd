<?php
declare(strict_types=1);
namespace App\Interfaces\Http\Middleware;

use App\Domain\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApiExternalRole
{
    public function handle($request, Closure $next) {
        if(Auth::user()->hasRole(User::$ROLE_API_EXTERNAL) === false || Auth::user()->active !== 1){
            abort(401);
        }

        return $next($request);
    }
}
