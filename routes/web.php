<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/demo/show/{id}', \App\Interfaces\Http\Front\Action\DemoShowAction::class)->name('web-demo-show');
//Route::post('/demo/store', \App\Interfaces\Http\Front\Action\DemoStoreAction::class)->name('web-demo-store');
