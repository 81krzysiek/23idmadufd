<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/v1/login', \App\Interfaces\Http\ApiExternal\Action\LoginAction::class)->name('api-login');


Route::group(['middleware' => ['jwt.verify', 'api.role']], function() {
    Route::post('/v1/employee/create', \App\Interfaces\Http\ApiExternal\Action\EmployeeCreateAction::class)->name('api-employee-create');
    Route::post('/v1/delegation/create', \App\Interfaces\Http\ApiExternal\Action\DelegationCreateAction::class)->name('api-delegation-create');
    Route::get('/v1/delegation/list/{identifier}', \App\Interfaces\Http\ApiExternal\Action\DelegationListByIdentifierAction::class)->name('api-delegation-list-by-identifier');
});

