<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Domain\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class CreateUserApiSeeder extends Seeder
{
    private string $userLogin  = 'api_user_2244';

    public function run()
    {
        if(!Role::where('name', User::$ROLE_API_EXTERNAL)->first()){
            Role::create(['name' => User::$ROLE_API_EXTERNAL]);
        }

        $user = User::where('login', $this->userLogin)->first();
        if (!$user){
            $user = User::create(['login' => $this->userLogin, 'password' => bcrypt('123rtpTest')]);
        }

        $user->assignRole(User::$ROLE_API_EXTERNAL);
        $user->save();
    }
}
