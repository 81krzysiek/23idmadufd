<?php
declare(strict_types=1);
namespace Database\Seeders;

use App\Domain\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    public function run()
    {
        $countries = [
            ['name' => 'Poland', 'code' => 'PL', 'amount' => 10],
            ['name' => 'Germany', 'code' => 'DE', 'amount' => 50],
            ['name' => 'United Kingdom', 'code' => 'GB', 'amount' => 75],
        ];

        foreach ($countries as $country){
            if(!Country::where('code', strtoupper($country['code']))->first()){

                Country::create($country);
            }
        }
    }
}
