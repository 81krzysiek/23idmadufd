<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('delegation_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('delegation_id');
            $table->index('delegation_id');
            $table->foreign('delegation_id')
                ->references('id')
                ->on('delegations')
                ->onDelete('cascade')
            ;
            $table->dateTime('start');
            $table->dateTime('end');
            $table->float('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('delegation_items');
    }
};
